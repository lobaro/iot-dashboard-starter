# IoT-Dashboard Starter

Small static fileserver to serve the [IoT-Dashboard](http://iot-dashboard.org) without any backend logic.

## Install and Run from NPM

Install the starter globally

    npm install -g iot-dashboard-starter
    
Run the server

    iot-dashboard
    
Open your browser at [http://localhost:8081](http://localhost:8081)


More about IoT-Dashboard at [http://iot-dashboard.org](http://iot-dashboard.org)
